using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI.Entities;

public class Team : IIntEntity
{
    public string Name { get; set; } = string.Empty;

    public string Logo { get; set; } = string.Empty;
    
    public int Point { get; set; }
    
    public int Position { get; set; }
    public int LeagueId { get; set; }
    public IEnumerable<Match> Matchs { get; set; }
}