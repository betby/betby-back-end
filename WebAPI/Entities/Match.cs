using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI.Entities;

public class Match : IIntEntity
{
    public int HomeTeamId { get; set; }
    public int VisitorTeamId { get; set; }
    
    public int LeagueId { get; set; }

    public int HomeScore { get; set; }
    
    public int VisitorScore { get; set; }

    public bool IsFinished { get; set; }

    public string StartDate { get; set; } = string.Empty;
    
    public float HomeOdd { get; set; }
    public float DrawOdd { get; set; }
    public float VisitorOdd { get; set; }
}