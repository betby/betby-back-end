﻿namespace WebAPI.Entities;

public class ThirdPartyRequests : IIntEntity
{
    public int RequestNumber { get; set; }

    public string Date { get; set; } = string.Empty;
}