namespace WebAPI.Entities;

public class Bet : IIntEntity
{
    public int UserId { get; set; }
    public bool BetAccepted { get; set; }
    public float BetAmount { get; set; }
    public float PotentialGain { get; set; }
    public float Odd { get; set; }
    public bool ResultObtained { get; set; }
    public bool IsWon { get; set; }
    public int MatchId { get; set; }
    public string MatchBetType { get; set; } = string.Empty;
}