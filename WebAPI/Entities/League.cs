﻿namespace WebAPI.Entities;

public class League : IIntEntity
{
    public string Logo { get; set; } = string.Empty;

    public string Name { get; set; } = string.Empty;
    public IEnumerable<Team> Teams { get; set; }
}