﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI.Entities;

public class IIntEntity
{
    [Key]
    public int Id { get; set; }
}