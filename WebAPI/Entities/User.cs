﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Entities;

public class User : IIntEntity
{
    public string UserName { get; set; } = string.Empty;
    public byte[] PasswordHash { get; set; }
    public byte[] PasswordSalt { get; set; }
    public string Role { get; set; } = string.Empty;
    public string AvatarColor { get; set; } = string.Empty;
    public string AvatarLabel { get; set; } = string.Empty;
    public float CurrencyAmount { get; set; }
    public bool IsActive { get; set; }
}