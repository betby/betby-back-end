using System.Text;
using AutoMapper;
using DatabaseServer;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Quartz;
using Swashbuckle.AspNetCore.Filters;
using WebAPI;
using WebAPI.CronJobs;
using WebAPI.Daos;
using WebAPI.Entities;
using WebAPI.Extensions;
using WebAPI.Managers;
using WebAPI.Models;
using WebAPI.Models.ThirdParty;
using WebAPI.Models.User;
using WebAPI.ThirdPartyAPI;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

// Setup the database connexion
builder.Services.AddDbContextFactory<DataContext>((options) =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"), optionsBuilder =>
    {
        optionsBuilder.EnableRetryOnFailure();
    });
});

// Fetch the Auto-Mapper configuration
var config = new MapperConfiguration(options =>
{
    options.AddProfile(new AutoMapperProfile());
});

// Mapper injection
var mapper = config.CreateMapper();
builder.Services.AddSingleton(mapper);

// Managers injection
builder.Services.AddSingleton<UserManager>();
builder.Services.AddSingleton<BetManager>();

// DAOs injection
builder.Services.TryAddGenericIntRepository<League, LeagueModel>();
builder.Services.TryAddGenericIntRepository<Team, TeamModel>();
builder.Services.TryAddGenericIntRepository<Match, MatchModel>();
builder.Services.TryAddGenericIntRepository<User, UserModel>();
builder.Services.TryAddGenericIntRepository<Bet, BetModel>();
builder.Services.TryAddGenericIntRepository<ThirdPartyRequests, ThirdPartyRequestsModel>();

builder.Services.TryAddTransient<IMatchRepository, MatchRepository>();
builder.Services.TryAddTransient<ITeamRepository, TeamRepository>();
builder.Services.TryAddTransient<IUserRepository, UserRepository>();
builder.Services.TryAddTransient<IBetRepository, BetRepository>();
builder.Services.TryAddTransient<IThirdPartyRepository, ThirdPartyRepository>();
builder.Services.TryAddTransient<IThirdPartyHttpClient, ThirdPartyHttpClient>();

// Add JWT Authentication
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                builder.Configuration.GetSection("AppSettings:Token").Value)),
            ValidateIssuer = false,
            ValidateAudience = false,
        };
    });

// Configure swaggerGen to accept auth token
builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        Description = "Standard Authorization header using the bearer scheme (\"Bearer {token}\")",
        In = ParameterLocation.Header,
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey
    });

    options.OperationFilter<SecurityRequirementsOperationFilter>();
});

//Configure cron jobs
builder.Services.AddQuartz(options =>
{
    options.UseMicrosoftDependencyInjectionJobFactory();
    var dailyJobKey = new JobKey("DailyJob");
    options.AddJob<DailyJob>(opts => opts.WithIdentity(dailyJobKey));
    
    var refreshJobKey = new JobKey("RefreshJob");
    options.AddJob<RefreshJob>(opts => opts.WithIdentity(refreshJobKey));

    options.AddTrigger(opts => opts
        .ForJob(dailyJobKey)
        .WithIdentity("DailyJob-EveryDay")
        .WithCronSchedule("0 0 0 * * ?"));

    options.AddTrigger(opts => opts
        .ForJob(dailyJobKey)
        .WithIdentity("DailyJob-Startup")
        .WithSimpleSchedule(SimpleScheduleBuilder.RepeatSecondlyForTotalCount(1)));
    
    options.AddTrigger(opts => opts
        .ForJob(refreshJobKey)
        .WithIdentity("RefreshJob-30Minutes")
        .WithCronSchedule("0 0/30 * * * ?"));
    
    options.AddTrigger(opts => opts
        .ForJob(refreshJobKey)
        .WithIdentity("RefreshJob-Startup")
        .WithSimpleSchedule(SimpleScheduleBuilder.RepeatMinutelyForTotalCount(1)));
});

builder.Services.AddQuartzHostedService(options =>
{
    options.WaitForJobsToComplete = true;
});

//Configure CORS
builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(a =>
    {
        a.AllowAnyHeader().AllowAnyMethod().AllowCredentials().WithOrigins("http://localhost:4200");
    });
});

var app = builder.Build();

using(var scope = app.Services.CreateScope())
{
    var dataContext = scope.ServiceProvider.GetRequiredService<DataContext>();
    dataContext.Database.EnsureCreated();
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.UseCors();

app.Run();
