﻿using AutoMapper;
using WebAPI.Entities;
using WebAPI.Models;
using WebAPI.Models.ThirdParty;
using WebAPI.Models.User;

namespace DatabaseServer;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<LeagueModel, League>().ReverseMap();
        CreateMap<MatchModel, Match>().ReverseMap();
        CreateMap<TeamModel, Team>().ReverseMap();
        CreateMap<UserModel, User>().ReverseMap();
        CreateMap<BetModel, Bet>().ReverseMap();
        CreateMap<ThirdPartyRequestsModel, ThirdPartyRequests>().ReverseMap();

        CreateMap<ThirdPartyLeague, LeagueModel>();
        CreateMap<ThirdPartyTeam, TeamModel>()
            .ForMember(x => x.Point, opt => opt.MapFrom( o => 0))
            .ForMember(x => x.LeagueId, opt => opt.MapFrom( o => 0));
        
        CreateMap<ThirdPartyMatch, MatchModel>()
            .ForMember(x => x.LeagueId, opt => opt.MapFrom( o => o.league.id))
            .ForMember(x => x.HomeTeamId, opt => opt.MapFrom(o => o.teams.home.id))
            .ForMember(x => x.VisitorTeamId, opt => opt.MapFrom(o => o.teams.away.id))
            .ForMember(x => x.HomeScore, opt => opt.MapFrom(o => o.scores.home))
            .ForMember(x => x.VisitorScore, opt => opt.MapFrom(o => o.scores.away))
            .ForMember(x => x.IsFinished, opt => opt.MapFrom(o => o.status.Short.Equals("FT")))
            .ForMember(x => x.StartDate, opt => opt.MapFrom(o => o.date));
    }
    
}