﻿using WebAPI.Models;

namespace WebAPI.ThirdPartyAPI;

public interface IThirdPartyHttpClient
{
    public void GetAllLeagues(int season);
    public void GetAllTeams(int season, int leagueId);
    public void GetAllMatches(int season, int leagueId);
    public void GetAllOdds(int season, int leagueId);
    public void UpdateStandings(int season, int leagueId);
}