using System.Globalization;
using AutoMapper;
using Newtonsoft.Json.Linq;
using WebAPI.Daos;
using WebAPI.Models;
using WebAPI.Models.ThirdParty;

namespace WebAPI.ThirdPartyAPI;

public class ThirdPartyHttpClient : IThirdPartyHttpClient
{
    private HttpClient _client = new HttpClient();
    private readonly IThirdPartyRepository _thirdPartyRepository;
    private readonly ITeamRepository _teamRepository;
    private readonly IMatchRepository _matchRepository;
    private readonly IGenericIntRepository<LeagueModel> _leagueRepository;
    private string _path;
    private IMapper _mapper;

    public ThirdPartyHttpClient(IThirdPartyRepository thirdPartyRepository, IMapper mapper, IMatchRepository matchRepository, ITeamRepository teamRepository, IGenericIntRepository<LeagueModel> leagueRepository)
    {
        _thirdPartyRepository = thirdPartyRepository;
        _mapper = mapper;
        _matchRepository = matchRepository;
        _teamRepository = teamRepository;
        _leagueRepository = leagueRepository;
        _client.DefaultRequestHeaders.Add("X-RapidAPI-Key", "c3850dba77msh3cef2bb40c6594ap18a8b9jsn6fa095f54a7c");
        _client.DefaultRequestHeaders.Add("X-RapidAPI-Host", "api-rugby.p.rapidapi.com");
    }

    public void GetAllLeagues(int season)
    {
        _path = $"https://api-rugby.p.rapidapi.com/leagues?season={season}";
        string date = DateTime.Today.ToString();
        
        if (!ValidateRequest(date)) return;

        HttpResponseMessage response = _client.GetAsync(_path).Result;

        JObject jsonResponse = JObject.Parse(response.Content.ReadAsStringAsync().Result);

        List<LeagueModel> models = _mapper.Map<List<LeagueModel>>(jsonResponse["response"].ToObject<List<ThirdPartyLeague>>());

        foreach (var model in models)
        {
            _leagueRepository.Add(model);
        }
    }
    
    public void GetAllTeams(int season, int leagueId)
    {
        _path = $"https://api-rugby.p.rapidapi.com/teams?season={season}&league={leagueId}";
        string date = DateTime.Today.ToString();
        
        if (!ValidateRequest(date)) return;

        HttpResponseMessage response = _client.GetAsync(_path).Result;

        JObject jsonResponse = JObject.Parse(response.Content.ReadAsStringAsync().Result);

        List<TeamModel> models = _mapper.Map<List<TeamModel>>(jsonResponse["response"].ToObject<List<ThirdPartyTeam>>());

        foreach (var model in models)
        {
            model.LeagueId = leagueId;
            _teamRepository.Add(model);
        }
        
    }

    public void GetAllMatches(int season, int leagueId)
    {
        _path = $"https://api-rugby.p.rapidapi.com/games?season={season}&league={leagueId}";
        string date = DateTime.Today.ToString();
        
        if (!ValidateRequest(date)) return;

        HttpResponseMessage response = _client.GetAsync(_path).Result;

        JObject jsonResponse = JObject.Parse(response.Content.ReadAsStringAsync().Result);

        List<ThirdPartyMatch> tpMatches = jsonResponse["response"].ToObject<List<ThirdPartyMatch>>();

        List<MatchModel> models = _mapper.Map<List<MatchModel>>(tpMatches);

        foreach (var model in models)
        {
            _matchRepository.Add(model);
        }
    }

    public void GetAllOdds(int season, int leagueId)
    {
        _path = $"https://api-rugby.p.rapidapi.com/odds?season={season}&league={leagueId}";
        string date = DateTime.Today.ToString();
        
        if (!ValidateRequest(date)) return;

        HttpResponseMessage response = _client.GetAsync(_path).Result;

        JObject jsonResponse = JObject.Parse(response.Content.ReadAsStringAsync().Result);

        List<ThirdPartyOdds> tpOdds = jsonResponse["response"].ToObject<List<ThirdPartyOdds>>();

        foreach (var odd in tpOdds)
        {
            var bookmaker = odd.bookmakers.Find(b => b.name.Equals("Unibet"));
            if (bookmaker == null)
            {
                continue;
            }

            var bet = bookmaker.bets.Find(b => b.name.Equals("3Way Result"));
            if (bet == null)
            {
                continue;
            }
            
            var match = _matchRepository.GetById(odd.game.id);

            if (match == null)
            {
                continue;
            }

            var draw = bet.values.Find(v => v.value.Equals("Draw"))?.odd;
            var home = bet.values.Find(v => v.value.Equals("Home"))?.odd;
            var visitor = bet.values.Find(v => v.value.Equals("Away"))?.odd;

            if (draw != null) match.DrawOdd = float.Parse(draw, CultureInfo.InvariantCulture);
            if (home != null) match.HomeOdd = float.Parse(home, CultureInfo.InvariantCulture);
            if (visitor != null) match.VisitorOdd = float.Parse(visitor, CultureInfo.InvariantCulture);
            _matchRepository.Add(match);
        }
    }

    public void UpdateStandings(int season, int leagueId)
    {
        _path = $"https://api-rugby.p.rapidapi.com/standings?season={season}&league={leagueId}";
        string date = DateTime.Today.ToString();

        if (!ValidateRequest(date)) return;
        
        HttpResponseMessage response = _client.GetAsync(_path).Result;

        JObject jsonResponse = JObject.Parse(response.Content.ReadAsStringAsync().Result);

        List<ThirdPartyStandings> models = jsonResponse["response"][0].ToObject<List<ThirdPartyStandings>>();

        foreach (var model in models)
        {
            var team = _teamRepository.GetById(model.team.id, leagueId);
            if (team == null) continue;
            
            team.Point = model.points;
            team.Position = model.position;
            _teamRepository.Add(team);
        }
    }

    private bool ValidateRequest(string date)
    {
        if (!_thirdPartyRepository.CanRequest(date))
        {
            Console.Out.Write("Too much third party calls today");
            return false;
        }
        
        _thirdPartyRepository.Increment(date);
        return true;
    }
}