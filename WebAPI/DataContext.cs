using Microsoft.EntityFrameworkCore;
using WebAPI.Entities;

namespace WebAPI;

public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<Match> Matches { get; set; }
        public DbSet<Team> Teams { get; set; }
        
        public DbSet<League> Leagues { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ThirdPartyRequests> ThirdPartyRequests { get; set; }
        public DbSet<Bet> Bets { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Match>().Property(m => m.Id).ValueGeneratedNever();
            modelBuilder.Entity<Team>().Property(t => t.Id).ValueGeneratedNever();
            modelBuilder.Entity<League>().Property(l => l.Id).ValueGeneratedNever();
            modelBuilder.Entity<Bet>().Property(l => l.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<Match>()
                .HasOne<Team>()
                .WithMany(t => t.Matchs)
                .HasForeignKey(m => new {m.HomeTeamId, m.LeagueId});
            
            modelBuilder.Entity<Match>()
                .HasOne<Team>()
                .WithMany(t => t.Matchs)
                .HasForeignKey(m => new {m.VisitorTeamId, m.LeagueId});

            modelBuilder.Entity<Team>()
                .HasOne<League>()
                .WithMany(l => l.Teams)
                .HasForeignKey(t => t.LeagueId);

            modelBuilder.Entity<Team>()
                .HasKey(key => new { key.Id, key.LeagueId });
        }
    }