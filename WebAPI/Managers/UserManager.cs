﻿using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;
using WebAPI.Daos;
using WebAPI.Enums;
using WebAPI.Models.User;

namespace WebAPI.Managers;

public class UserManager
{
    private readonly ILogger<UserManager> _logger;
        private readonly IConfiguration _configuration;
        private readonly Random rand = new Random();
        private readonly IUserRepository _userRepository;

        public UserManager(ILogger<UserManager> logger, IConfiguration configuration, IUserRepository userRepository)
        {
            _logger = logger;
            _configuration = configuration;
            _userRepository = userRepository;
        }

        public ConnectionResultModel Login(UserConnectionModel request)
        {
            UserModel? user = _userRepository.GetUserByName(request.UserName);
            
            var emptyResult = new ConnectionResultModel()
            {
                Token = "",
                AvatarColor = "",
                AvatarLabel = "",
                Role = "",
                UserName = "",
                Id = 0,
                CurrencyAmount = 0f
            };
            
            if (user == null)
            {
                return emptyResult;
            }

            if (!VerifyPasswordHash(request.Password, user.PasswordHash, user.PasswordSalt))
            {
                return emptyResult;
            }

            string token = CreateToken(user);
            
            return new ConnectionResultModel()
            {
                Token = token,
                AvatarColor = user.AvatarColor,
                AvatarLabel = user.AvatarLabel,
                Role = user.Role,
                UserName = user.UserName,
                Id = user.Id,
                CurrencyAmount = user.CurrencyAmount
            };
        }

        public bool Register(UserRegisterModel request)
        {
            if (!_userRepository.IsUsernameAvailable(request.UserName))
            {
                return false;
            }
            
            CreatePasswordHash(request.Password, out byte[] passwordHash, out byte[] passwordSalt);
            UserModel user = new UserModel
            {
                UserName = request.UserName,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                Role = Role.User,
                AvatarColor = ColorTranslator.ToHtml(
                    Color.FromArgb(rand.Next(256),rand.Next(256),rand.Next(256))),
                AvatarLabel = CreateUserLabel(request.UserName),
                CurrencyAmount = 100f
            };
            
            _userRepository.Add(user);

            return true;
        }

        public List<UserModel> GetUsers()
        {
            return _userRepository.GetAll();
        }

        public bool DeleteUser(int userId)
        {
            return _userRepository.DisableUser(userId);
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computedHash.SequenceEqual(passwordHash);
            }
        }
        
        private string CreateToken(UserModel user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Role, user.Role)
            };

            var key = new SymmetricSecurityKey(
                System.Text.Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value));

            var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddHours(10),
                signingCredentials: cred);

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);
            
            return jwt;
        }

        private string CreateUserLabel(string username)
        {
            var spliced = username.ToUpperInvariant().Split('.');

            if (spliced.Length > 1)
            {
                return $"{spliced[0][0]}{spliced[1][0]}";
            }

            return $"{spliced[0][0]}";
        }

        public float GetCurrency(int userId)
        {
            var user = _userRepository.GetById(userId);
            if (user == null) return 0;
            return user.CurrencyAmount;
        }
}