﻿using System.Globalization;
using WebAPI.Daos;
using WebAPI.Enums;
using WebAPI.Models;
using WebAPI.Models.User;

namespace WebAPI.Managers;

public class BetManager
{
    private readonly IBetRepository _betRepository;
    private readonly IMatchRepository _matchRepository;
    private readonly IUserRepository _userRepository;

    public BetManager(IBetRepository betRepository, IUserRepository userRepository, IMatchRepository matchRepository)
    {
        _betRepository = betRepository;
        _userRepository = userRepository;
        _matchRepository = matchRepository;
    }

    public IEnumerable<BetModel> GetAllBets()
    {
        return _betRepository.GetAll();
    }
    
    public IEnumerable<BetModel> GetAllBetsByUserId( int id)
    {
        return _betRepository.GetAllBetByUserId(id);
    }

    public BetModel? GetBet(int id)
    {
       return _betRepository.GetById(id);
    }

    public BetModel Add(BetRequestModel betRequest)
    {
        BetModel bet = new BetModel()
        {
            BetAccepted = false,
            BetAmount = betRequest.MoneyBet,
            IsWon = false,
            Odd = 0,
            PotentialGain = 0,
            ResultObtained = false,
            MatchId = betRequest.MatchId,
            MatchBetType = betRequest.MatchBetType,
            UserId = betRequest.UserId
        };

        if (betRequest.MoneyBet <= 0)
        {
            return bet;
        }
        
        UserModel? user = _userRepository.GetById(betRequest.UserId);
        if (user == null || user.CurrencyAmount < betRequest.MoneyBet)
        {
            return bet;
        }

        MatchModel? match = _matchRepository.GetById(betRequest.MatchId);
        if (match == null || DateTime.ParseExact(match.StartDate, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture) < DateTime.Now)
        {
            return bet;
        }
        
        switch (betRequest.MatchBetType)
        {
            case MatchBetType.Home:
                bet.Odd = match.HomeOdd;
                break;
            
            case MatchBetType.Draw:
                bet.Odd = match.DrawOdd;
                break;
            
            case MatchBetType.Visitor:
                bet.Odd = match.VisitorOdd;
                break;
            
            default:
                return bet;
        }
        
        bet.BetAccepted = true;
        bet.PotentialGain = betRequest.MoneyBet * bet.Odd;

        bet = _betRepository.Add(bet);
        _userRepository.ChangeCurrencyAmount(betRequest.UserId ,-betRequest.MoneyBet);

        return bet;
    }

    public bool DeleteBet(int id)
    {
        return false;
    }

    public void RefreshBets()
    {
        var bets = _betRepository.GetAll().Where(b => b.ResultObtained == false);
        foreach (var bet in bets)
        {
            var match = _matchRepository.GetById(bet.MatchId);
            
            if (match == null) continue;
            if (match.IsFinished == false) continue;

            bet.ResultObtained = true;

            string winnerType;
            
            if (match.HomeScore > match.VisitorScore)
            {
                winnerType = MatchBetType.Home;
            } 
            else if (match.HomeScore < match.VisitorScore)
            {
                winnerType = MatchBetType.Visitor;
            }
            else
            {
                winnerType = MatchBetType.Draw;
            }

            bet.IsWon = bet.MatchBetType == winnerType;
            
            _betRepository.Add(bet);

            if (!bet.IsWon) continue;
            
            var user = _userRepository.GetById(bet.UserId);
            if (user == null) continue;
            
            user.CurrencyAmount += bet.PotentialGain;
            _userRepository.Add(user);
        }
    }
}