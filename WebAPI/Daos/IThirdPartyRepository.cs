﻿using WebAPI.Models;

namespace WebAPI.Daos;

public interface IThirdPartyRepository : IGenericIntRepository<ThirdPartyRequestsModel>
{
    public bool CanRequest(string date);
    public bool Increment(string date);
    public bool IsDayInitialized(string date);
    public void InitializeDay(string date);
    public int RequestsRemaining(string date);
}