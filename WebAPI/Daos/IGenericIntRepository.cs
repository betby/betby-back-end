﻿using WebAPI.Models;

namespace WebAPI.Daos;

public interface IGenericIntRepository<TModel>
    where TModel: IIntModel, new()
{
    public TModel? GetById(int id);
    public List<TModel> GetAll();
    public TModel? Add(TModel model);
    public bool Delete(int id);
}