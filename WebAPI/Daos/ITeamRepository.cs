﻿using WebAPI.Models;

namespace WebAPI.Daos;

public interface ITeamRepository : IGenericIntRepository<TeamModel>
{
    public IEnumerable<TeamModel> GetAllTeamsByLeagueId(int id);
    public TeamModel? GetById(int id, int leagueId);
    public bool Delete(int id, int leagueId);
}