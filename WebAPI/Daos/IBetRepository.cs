using WebAPI.Models;

namespace WebAPI.Daos;

public interface IBetRepository : IGenericIntRepository<BetModel>
{
    public IEnumerable<BetModel> GetAllBetByUserId(int id);
}