using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Daos;

public class BetRepository : IBetRepository
{
    private readonly IGenericIntRepository<BetModel> _betRepository;
    private readonly IDbContextFactory<DataContext> _contextFactory;
    private readonly IMapper _mapper;
    private ILogger<BetRepository> _logger;
    
    public BetRepository(IGenericIntRepository<BetModel> betRepository, IDbContextFactory<DataContext> contextFactory, IMapper mapper, ILogger<BetRepository> logger)
    {
        _betRepository = betRepository;
        _contextFactory = contextFactory;
        _mapper = mapper;
        _logger = logger;
    }
    
    
    public BetModel? GetById(int id)
    {
        return _betRepository.GetById(id);
    }

    public List<BetModel> GetAll()
    {
        return _betRepository.GetAll();
    }

    public BetModel? Add(BetModel model)
    {
        return _betRepository.Add(model);
    }

    public bool Delete(int id)
    {
        return _betRepository.Delete(id);
    }

    public IEnumerable<BetModel> GetAllBetByUserId(int id)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var entity = context.Bets.Where(t => t.UserId == id);

            return _mapper.Map<List<BetModel>>(entity);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetAllBetByUserId: Bet");
            return new List<BetModel>();
        }
    }
}