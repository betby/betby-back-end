﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WebAPI.Entities;
using WebAPI.Models;

namespace WebAPI.Daos;

public class GenericIntRepository<TEntity, TModel> : IGenericIntRepository<TModel>
        where TEntity : IIntEntity, new()
        where TModel : IIntModel, new()
    {
        private static readonly string _entityName = nameof(TEntity);
    
        private readonly ILogger<GenericIntRepository<TEntity, TModel>> _logger;
        private readonly IDbContextFactory<DataContext> _contextFactory;
        private readonly IMapper _mapper;
        
        public GenericIntRepository(
            IDbContextFactory<DataContext> contextFactory, 
            IMapper mapper,
            ILogger<GenericIntRepository<TEntity, TModel>> logger)
        {
            _contextFactory = contextFactory;
            _mapper = mapper;
            _logger = logger;
        }
        
        public TModel? GetById(int id)
        {
            try
            {
                using var context = _contextFactory.CreateDbContext();
                var entity = context.Set<TEntity>().Find(id);
                
                return _mapper.Map<TModel>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetById: {EntityName}", _entityName);
                return default;
            }
        }

        public List<TModel> GetAll()
        {
            try
            {
                using var context = _contextFactory.CreateDbContext();
                var entity = context.Set<TEntity>();
                
                return _mapper.Map<List<TModel>>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetAll: {EntityName}", _entityName);
                return new List<TModel>();
            }
        }

        public TModel? Add(TModel model)
        {
            try
            {
                using var context = _contextFactory.CreateDbContext();

                TEntity entity = _mapper.Map<TEntity>(model);

                if (context.Set<TEntity>().Contains(entity))
                {
                    context.Set<TEntity>().Update(entity);
                }
                else
                {
                    context.Set<TEntity>().Add(entity);
                }
                
                context.SaveChanges();

                return _mapper.Map<TModel>(entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Add: {EntityName}", _entityName);
                return null;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using var context = _contextFactory.CreateDbContext();
                TEntity? entity = context.Set<TEntity>().Find(id);

                if (entity == null)
                {
                    return false;
                }

                context.Set<TEntity>().Remove(entity);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Delete: {EntityName}", _entityName);
                return false;
            }
        }
    }