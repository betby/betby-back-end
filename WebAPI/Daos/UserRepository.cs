﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WebAPI.Entities;
using WebAPI.Models.User;

namespace WebAPI.Daos;

public class UserRepository : IUserRepository
{
    private readonly ILogger<UserRepository> _logger;
    private readonly IDbContextFactory<DataContext> _contextFactory;
    private readonly IMapper _mapper;

    public UserRepository(ILogger<UserRepository> logger, IDbContextFactory<DataContext> contextFactory, IMapper mapper, IGenericIntRepository<UserModel> userRepository)
    {
        _logger = logger;
        _contextFactory = contextFactory;
        _mapper = mapper;
    }

    public UserModel? GetById(int id)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var entity = context.Users.FirstOrDefault(e => e.Id == id && e.IsActive == true);

            return _mapper.Map<UserModel>(entity);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetByIdAsync: User");
            return default;
        }
    }

    public List<UserModel> GetAll()
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var entity = context.Users.Where(e => e.IsActive == true);
                
            return _mapper.Map<List<UserModel>>(entity);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetAll: User");
            return new List<UserModel>();
        }
    }

    public UserModel? Add(UserModel model)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var user = _mapper.Map<User>(model);
            user.IsActive = true;
            context.Users.Add(user);
            context.SaveChanges();
            return _mapper.Map<UserModel>(user);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Add: User");
            return null;
        }
    }

    public bool Delete(int id)
    {
        return DisableUser(id);
    }

    public UserModel? GetUserByName(string username)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var user = context.Users.FirstOrDefault( e => e.UserName == username && e.IsActive == true);
                
            return _mapper.Map<UserModel>(user);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetUserByName: User");
            return default;
        }
    }

    public bool IsUsernameAvailable(string username)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var res = context.Users.All( e => e.UserName != username || e.IsActive == false);
                
            return res;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "IsUsernameTaken: User");
            return default;
        }
    }

    public bool DisableUser(int id)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var user = context.Users.FirstOrDefault( e => e.Id == id);
            if (user == null)
            {
                return false;
            }

            user.IsActive = false;
            context.SaveChanges();
                
            return true;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "DisableUser: User");
            return default;
        }
    }

    public void ChangeCurrencyAmount(int id, float amount)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var entity = context.Users.FirstOrDefault(e => e.Id == id && e.IsActive == true);
            if (entity == null) return;
            
            entity.CurrencyAmount += amount;
            context.SaveChanges();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetByIdAsync: User");
        }
    }
}