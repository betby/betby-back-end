﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WebAPI.Entities;
using WebAPI.Models;

namespace WebAPI.Daos;

public class TeamRepository : ITeamRepository
{
    private readonly IGenericIntRepository<TeamModel> _teamRepository;
    private readonly IDbContextFactory<DataContext> _contextFactory;
    private readonly IMapper _mapper;
    private ILogger<MatchRepository> _logger;

    public TeamRepository(IGenericIntRepository<TeamModel> teamRepository, IDbContextFactory<DataContext> contextFactory, IMapper mapper, ILogger<MatchRepository> logger)
    {
        _teamRepository = teamRepository;
        _contextFactory = contextFactory;
        _mapper = mapper;
        _logger = logger;
    }

    public TeamModel? GetById(int id, int leagueId)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var entity = context.Teams.Find(id, leagueId);
                
            return _mapper.Map<TeamModel>(entity);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetById: Team");
            return default;
        }
    }

    public TeamModel? GetById(int id)
    {
        return null;
    }

    public List<TeamModel> GetAll()
    {
        return _teamRepository.GetAll();
    }

    public TeamModel? Add(TeamModel model)
    {
        return _teamRepository.Add(model);
    }

    public bool Delete(int id)
    {
        return false;
    }

    public bool Delete(int id, int leagueId)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            Team? entity = context.Teams.Find(id, leagueId);

            if (entity == null)
            {
                return false;
            }

            context.Teams.Remove(entity);
            context.SaveChanges();
            return true;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Delete: Teams");
            return false;
        }
    }

    public IEnumerable<TeamModel> GetAllTeamsByLeagueId(int id)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var entity = context.Teams.Where(t => t.LeagueId == id);

            return _mapper.Map<List<TeamModel>>(entity);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetAllByTeamId: Team");
            return new List<TeamModel>();
        }
    }
}