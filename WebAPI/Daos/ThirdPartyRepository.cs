﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Daos;

public class ThirdPartyRepository : IThirdPartyRepository
{
    private readonly IGenericIntRepository<ThirdPartyRequestsModel> _thirdPartyRepository;
    private readonly IDbContextFactory<DataContext> _contextFactory;
    private readonly ILogger<ThirdPartyRepository> _logger;

    public ThirdPartyRepository(IGenericIntRepository<ThirdPartyRequestsModel> thirdPartyRepository, IDbContextFactory<DataContext> contextFactory, ILogger<ThirdPartyRepository> logger)
    {
        _thirdPartyRepository = thirdPartyRepository;
        _contextFactory = contextFactory;
        _logger = logger;
    }

    public ThirdPartyRequestsModel? GetById(int id)
    {
        return _thirdPartyRepository.GetById(id);
    }

    public List<ThirdPartyRequestsModel> GetAll()
    {
        return _thirdPartyRepository.GetAll();
    }

    public ThirdPartyRequestsModel? Add(ThirdPartyRequestsModel model)
    {
        return _thirdPartyRepository.Add(model);
    }

    public bool Delete(int id)
    {
        return _thirdPartyRepository.Delete(id);
    }

    public bool CanRequest(string date)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var entity = context.ThirdPartyRequests.FirstOrDefault(t => t.Date == date);

            if (entity != null) return entity.RequestNumber < 100;

            return false;

        }
        catch (Exception e)
        {
            _logger.LogError(e, "CanRequest: ThirdPartyRequest");
            return false;
        }
    }

    public bool Increment(string date)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var entity = context.ThirdPartyRequests.FirstOrDefault(t => t.Date == date);

            if (entity == null) return false;

            entity.RequestNumber++;

            context.SaveChanges();

            return true;

        }
        catch (Exception e)
        {
            _logger.LogError(e, "Increment: ThirdPartyRequest");
            return false;
        }
    }

    public bool IsDayInitialized(string date)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var entity = context.ThirdPartyRequests.FirstOrDefault(t => t.Date == date);

            return entity != null;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "IsDayInitialized: ThirdPartyRequest");
            return false;
        }
    }

    public void InitializeDay(string date)
    {
        Add(new ThirdPartyRequestsModel()
        {
            Date = DateTime.Today.ToString(),
            RequestNumber = 0
        });
    }

    public int RequestsRemaining(string date)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var entity = context.ThirdPartyRequests.FirstOrDefault(t => t.Date == date);
            if (entity == null)
            {
                return 0;
            }

            return 100 - entity.RequestNumber;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "RequestsRemaining: ThirdPartyRequest");
            return 0;
        }
    }
}