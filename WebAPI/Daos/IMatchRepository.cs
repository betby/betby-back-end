﻿using WebAPI.Models;

namespace WebAPI.Daos;

public interface IMatchRepository : IGenericIntRepository<MatchModel>
{
    public List<MatchModel> GetAllByTeamId(int id);
    public List<MatchModel> GetAllByDate(string date);
    public List<MatchModel> GetAllByDate(string date, int leagueId);
    public List<MatchModel> GetAllBeforeDate(DateTime date, int leagueId);
    public List<MatchModel> GetAllAfterDate(DateTime date, int leagueId);
}