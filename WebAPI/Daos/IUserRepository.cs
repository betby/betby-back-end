﻿using WebAPI.Models.User;

namespace WebAPI.Daos;

public interface IUserRepository : IGenericIntRepository<UserModel>
{
    public UserModel? GetUserByName(string username);
    public bool IsUsernameAvailable(string username);
    public bool DisableUser(int id);
    public void ChangeCurrencyAmount(int id, float amount);
}