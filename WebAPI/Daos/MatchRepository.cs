﻿using System.Globalization;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WebAPI.Entities;
using WebAPI.Enums;
using WebAPI.Models;

namespace WebAPI.Daos;

public class MatchRepository : IMatchRepository
{
    private readonly IGenericIntRepository<MatchModel> _matchRepository;
    private readonly ITeamRepository _teamRepository;
    private readonly IDbContextFactory<DataContext> _contextFactory;
    private readonly IMapper _mapper;
    private ILogger<MatchRepository> _logger;

    public MatchRepository(IGenericIntRepository<MatchModel> matchRepository, IMapper mapper, IDbContextFactory<DataContext> contextFactory, ILogger<MatchRepository> logger, ITeamRepository teamRepository)
    {
        _matchRepository = matchRepository;
        _mapper = mapper;
        _contextFactory = contextFactory;
        _logger = logger;
        _teamRepository = teamRepository;
    }

    public MatchModel? GetById(int id)
    {
        return _matchRepository.GetById(id);
    }

    public List<MatchModel> GetAll()
    {
        return _matchRepository.GetAll();
    }

    public MatchModel? Add(MatchModel model)
    {
        return _matchRepository.Add(model);
    }

    public bool Delete(int id)
    {
        return _matchRepository.Delete(id);
    }
    
    public List<MatchModel> GetAllByTeamId(int id)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            var entity = context.Matches.Where(m => m.HomeTeamId == id || m.VisitorTeamId == id);

            return _mapper.Map<List<MatchModel>>(entity);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetAllByTeamId: Match");
            return new List<MatchModel>();
        }
    }

    public List<MatchModel> GetAllByDate(string date)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            List<Match> matches = new List<Match>();

            foreach (var match in context.Matches)
            {
                if (IsSameDay(match, date))
                {
                    matches.Add(match);
                }
            }

            return _mapper.Map<List<MatchModel>>(matches);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetAllByDate: Match");
            return new List<MatchModel>();
        }
    }

    public List<MatchModel> GetAllByDate(string date, int leagueId)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            List<Match> matches = new List<Match>();

            foreach (var match in context.Matches)
            {
                if (!IsSameDay(match, date)) continue;

                if (match.LeagueId == leagueId)
                {
                    matches.Add(match);   
                }
            }

            return _mapper.Map<List<MatchModel>>(matches);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetAllByDate: Match");
            return new List<MatchModel>();
        }
    }

    public List<MatchModel> GetAllBeforeDate(DateTime date, int leagueId)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            List<Match> matches = new List<Match>();

            foreach (var match in context.Matches)
            {
                if (!(ParseDate(match.StartDate) < date)) continue;

                if (match.LeagueId == leagueId)
                {
                    matches.Add(match);   
                }
            }

            return _mapper.Map<List<MatchModel>>(matches);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetAllByDate: Match");
            return new List<MatchModel>();
        }
    }

    public List<MatchModel> GetAllAfterDate(DateTime date, int leagueId)
    {
        try
        {
            using var context = _contextFactory.CreateDbContext();
            List<Match> matches = new List<Match>();

            foreach (var match in context.Matches)
            {
                if (!(ParseDate(match.StartDate) > date.AddDays(1))) continue;

                if (match.LeagueId == leagueId)
                {
                    matches.Add(match);   
                }
            }

            return _mapper.Map<List<MatchModel>>(matches);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetAllByDate: Match");
            return new List<MatchModel>();
        }
    }

    private bool IsSameDay(Match match, string date)
    {
        var matchDate = ParseDate(match.StartDate);
        matchDate = matchDate.AddHours(matchDate.Hour * -1).AddMinutes(matchDate.Minute * -1).AddSeconds(matchDate.Second * -1);
        return matchDate.ToString().Equals(date);
    }

    private DateTime ParseDate(string date)
    {
        return DateTime.ParseExact(date, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
    }
}