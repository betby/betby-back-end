﻿using System.Runtime.Serialization;

namespace WebAPI.Models;

[DataContract]
public class IIntModel
{
    public int Id { get; set; }
}