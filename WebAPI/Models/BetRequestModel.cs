using System.Runtime.Serialization;
using WebAPI.Enums;

namespace WebAPI.Models;

public class BetRequestModel
{
    public int UserId { get; set; }
    public int MoneyBet { get; set; }
    public int MatchId { get; set; }
    public string MatchBetType { get; set; }
}