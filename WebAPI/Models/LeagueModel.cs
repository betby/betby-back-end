﻿using System.Runtime.Serialization;

namespace WebAPI.Models;

public class LeagueModel : IIntModel
{

    public string Logo { get; set; } = string.Empty;

    public string Name { get; set; } = string.Empty;
    
    public IEnumerable<TeamModel> Teams { get; set; }

}