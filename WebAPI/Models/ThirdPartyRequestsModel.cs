﻿namespace WebAPI.Models;

public class ThirdPartyRequestsModel : IIntModel
{
    public int RequestNumber { get; set; }
    public string Date { get; set; } = string.Empty;
}