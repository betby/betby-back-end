﻿namespace WebAPI.Models.User;

public class ConnectionResultModel : IIntModel
{
    public string Token { get; set; } = string.Empty;
    
    public string UserName { get; set; } = string.Empty;
    
    public string Role { get; set; } = string.Empty;
    
    public string AvatarColor { get; set; } = string.Empty;
    
    public string AvatarLabel { get; set; } = string.Empty;
    
    public float CurrencyAmount { get; set; }

}