﻿namespace WebAPI.Models.User;

public class UserConnectionModel
{
    public string UserName { get; set; } = string.Empty;
    
    public string Password { get; set; } = string.Empty;
}