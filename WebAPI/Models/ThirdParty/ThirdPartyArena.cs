﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartyArena : ThirdPartyModel
{
    public int? capacity { get; set; }
    public string location { get; set; }
    public string name { get; set; }
}