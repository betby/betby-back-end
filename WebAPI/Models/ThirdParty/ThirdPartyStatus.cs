﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartyStatus
{
    public string Long { get; set; }
    public string Short { get; set; }
}