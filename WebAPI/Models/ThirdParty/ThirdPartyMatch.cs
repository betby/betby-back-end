﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartyMatch : ThirdPartyModel
{
    public ThirdPartyCountry country { get; set; }
    public string date { get; set; }
    public int id { get; set; }
    public ThirdPartyLeague league { get; set; }
    public ThirdPartyPeriods periods { get; set; }
    public ThirdPartyScores scores { get; set; }
    public ThirdPartyStatus status { get; set; }
    public ThirdPartySimplifiedTeams teams { get; set; }
    public string time { get; set; }
    public int? timestamp { get; set; }
    public string timezone { get; set; }
    public string week { get; set; }
}