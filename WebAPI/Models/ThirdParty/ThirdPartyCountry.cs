﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartyCountry : ThirdPartyModel
{
    public int? id { get; set; }
    public string logo { get; set; }
    public string name { get; set; }
}