﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartyLeague : ThirdPartyModel
{
    public ThirdPartyCountry country { get; set; }
    public int id { get; set; }
    public string logo { get; set; }
    public string name { get; set; }
    public List<ThirdPartySeason> seasons { get; set; }
    public string type { get; set; }
}