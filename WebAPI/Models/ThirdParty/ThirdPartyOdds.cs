﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartyOdds
{
    public List<Bookmaker> bookmakers { get; set; }
    public TPGame game { get; set; }
    public DateTime update { get; set; }
}

public class TPBet
{
    public int id { get; set; }
    public string name { get; set; }
    public List<Value> values { get; set; }
}

public class Bookmaker
{
    public List<TPBet> bets { get; set; }
    public int id { get; set; }
    public string name { get; set; }
}

public class TPGame
{
    public int id { get; set; }
}

public class Value
{
    public string odd { get; set; }
    public string value { get; set; }
}