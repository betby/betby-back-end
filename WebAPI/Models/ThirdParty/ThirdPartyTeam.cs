﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartyTeam : ThirdPartyModel
{
    public int id { get; set; }
    public ThirdPartyArena arena { get; set; }
    public ThirdPartyCountry country { get; set; }
    public int? founded { get; set; }
    public string logo { get; set; }
    public string name { get; set; }
    public bool national { get; set; }
}