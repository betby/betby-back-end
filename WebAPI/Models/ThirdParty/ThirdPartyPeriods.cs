﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartyPeriods
{
    public ThirdPartyPeriod first { get; set; }
    public ThirdPartyPeriod overtime { get; set; }
    public ThirdPartyPeriod second { get; set; }
    public ThirdPartyPeriod second_overtime { get; set; }
}