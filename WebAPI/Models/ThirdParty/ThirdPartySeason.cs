﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartySeason : ThirdPartyModel
{
    public bool current { get; set; }
    public string end { get; set; }
    public int season { get; set; }
    public string start { get; set; }
}