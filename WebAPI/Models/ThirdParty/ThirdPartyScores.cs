﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartyScores
{
    public int? away { get; set; }
    public int? home { get; set; }
}