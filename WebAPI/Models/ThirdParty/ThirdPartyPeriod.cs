﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartyPeriod
{
    public int? away { get; set; }
    public int? home { get; set; }
}