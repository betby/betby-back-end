﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartyStandings
{
    public ThirdPartySimplifiedTeam team { get; set; }
    public int points { get; set; }
    public int position { get; set; }
}