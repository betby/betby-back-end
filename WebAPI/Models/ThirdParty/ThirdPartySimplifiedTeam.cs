﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartySimplifiedTeam
{
    public int id { get; set; }
    public string name { get; set; }
    public string logo { get; set; }
}