﻿namespace WebAPI.Models.ThirdParty;

public class ThirdPartySimplifiedTeams
{
    public ThirdPartySimplifiedTeam home { get; set; }
    public ThirdPartySimplifiedTeam away { get; set; }
}