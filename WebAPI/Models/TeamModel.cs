﻿using System.Runtime.Serialization;

namespace WebAPI.Models;

public class TeamModel : IIntModel
{
    public string Name { get; set; } = string.Empty;
    
    public string Logo { get; set; } = string.Empty;
    public int Point { get; set; }
    public int Position { get; set; }
    public int LeagueId { get; set; }
    
    public IEnumerable<MatchModel> Matchs { get; set; }
    
}