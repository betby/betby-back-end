﻿namespace WebAPI.Enums;

public static class RoleRestriction
{
    public const string Admin = Role.Admin;
    public const string User = $"{Role.Admin},{Role.User}";
}