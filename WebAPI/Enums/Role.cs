﻿namespace WebAPI.Enums;

public static class Role
{
    public const string Admin = "ADM";
    public const string User = "USR";
}