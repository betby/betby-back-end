﻿namespace WebAPI.Enums;

public class MatchBetType
{
    public const string Home = "Home";
    public const string Draw = "Draw";
    public const string Visitor = "Visitor";
}