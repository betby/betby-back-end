﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using WebAPI.Daos;
using WebAPI.Entities;
using WebAPI.Models;

namespace WebAPI.Extensions;

public static class ServiceCollectionExtension
{
    public static void TryAddGenericIntRepository<TEntity, TModel>(this IServiceCollection services)
        where TEntity : IIntEntity, new()
        where TModel : IIntModel, new()
    {
        services.TryAddTransient<IGenericIntRepository<TModel>, GenericIntRepository<TEntity, TModel>>();
    }
}