using Quartz;
using WebAPI.Daos;
using WebAPI.Managers;
using WebAPI.Models;
using WebAPI.ThirdPartyAPI;

namespace WebAPI.CronJobs;

public class DailyJob : IJob
{
    private readonly IThirdPartyRepository _thirdPartyRepository;
    private readonly IThirdPartyHttpClient _thirdPartyHttpClient;
    private readonly BetManager _betManager;

    public DailyJob(IThirdPartyRepository thirdPartyRepository, IThirdPartyHttpClient thirdPartyHttpClient, BetManager betManager)
    {
        _thirdPartyRepository = thirdPartyRepository;
        _thirdPartyHttpClient = thirdPartyHttpClient;
        _betManager = betManager;
    }

    public Task Execute(IJobExecutionContext context)
    {
        string date = DateTime.Today.ToString();

        if (_thirdPartyRepository.IsDayInitialized(date))
        {
            Console.Out.WriteLine("Daily Job : Data already fetched");
            return Task.CompletedTask;
        }
        
        _thirdPartyRepository.InitializeDay(date);
        
        _thirdPartyHttpClient.GetAllLeagues(2022);
        
        _thirdPartyHttpClient.GetAllTeams(2022, 16);
        _thirdPartyHttpClient.UpdateStandings(2022, 16);
        _thirdPartyHttpClient.GetAllMatches(2022, 16);
        _thirdPartyHttpClient.GetAllOdds(2022,16);
        
        _thirdPartyHttpClient.GetAllTeams(2022, 54);
        _thirdPartyHttpClient.UpdateStandings(2022, 54);
        _thirdPartyHttpClient.GetAllMatches(2022, 54);
        _thirdPartyHttpClient.GetAllOdds(2022,54);
        
        _betManager.RefreshBets();
        
        Console.Out.WriteLine("Daily Job : Requested");

        return Task.CompletedTask;
    }
}