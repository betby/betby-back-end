﻿using System.Globalization;
using Microsoft.IdentityModel.Tokens;
using Quartz;
using WebAPI.Daos;
using WebAPI.Managers;
using WebAPI.ThirdPartyAPI;

namespace WebAPI.CronJobs;

public class RefreshJob : IJob
{
    private readonly IThirdPartyRepository _thirdPartyRepository;
    private readonly IThirdPartyHttpClient _thirdPartyHttpClient;
    private readonly IMatchRepository _matchRepository;
    private readonly BetManager _betManager;

    public RefreshJob(IThirdPartyRepository thirdPartyRepository, IThirdPartyHttpClient thirdPartyHttpClient, IMatchRepository matchRepository, BetManager betManager)
    {
        _thirdPartyRepository = thirdPartyRepository;
        _thirdPartyHttpClient = thirdPartyHttpClient;
        _matchRepository = matchRepository;
        _betManager = betManager;
    }
    
    public Task Execute(IJobExecutionContext context)
    {
        string date = DateTime.Today.ToString();

        if (!_thirdPartyRepository.CanRequest(date))
        {
            Console.Out.WriteLine("Refresh Job : No more ThirdParty calls available");
            return Task.CompletedTask;
        }
        
        if (!_thirdPartyRepository.IsDayInitialized(date))
        {
            Console.Out.WriteLine("Refresh Job : Day not initialized");
            return Task.CompletedTask;
        }

        var todayMatches = _matchRepository.GetAllByDate(DateTime.Today.ToString());
        
        if (todayMatches.IsNullOrEmpty())
        {
            Console.Out.WriteLine("Refresh Job : No Matches Today");
            return Task.CompletedTask;
        }

        DateTime firstMatchStart = DateTime.Today.AddDays(1).Subtract(TimeSpan.FromSeconds(1));
        DateTime lastMatchStart = DateTime.Today;
        
        foreach (var match in todayMatches)
        {
            var dateTime =  DateTime.ParseExact(match.StartDate, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            if (dateTime < firstMatchStart) firstMatchStart = dateTime;
            if (dateTime > lastMatchStart) lastMatchStart = dateTime;
        }

        DateTime lastMatchEnd = lastMatchStart.AddMinutes(100);
        
        if (firstMatchStart > DateTime.Now)
        {
            Console.Out.WriteLine("Refresh Job : Matches didn't start yet");
            return Task.CompletedTask;
        }
        
        if (lastMatchEnd < DateTime.Now)
        {
            Console.Out.WriteLine("Refresh Job : Every Matches are finished");
            return Task.CompletedTask;
        }
        
        _thirdPartyHttpClient.GetAllOdds(2022,16);
        _thirdPartyHttpClient.GetAllMatches(2022, 16);
        _thirdPartyHttpClient.UpdateStandings(2022, 54);
        _thirdPartyHttpClient.GetAllOdds(2022,54);
        
        _betManager.RefreshBets();

        return Task.CompletedTask;
    }
}