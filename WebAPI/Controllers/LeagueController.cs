﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Daos;
using WebAPI.Entities;
using WebAPI.Enums;
using WebAPI.Managers;
using WebAPI.Models;

namespace WebAPI.Controllers;

[ApiController]
[Route("[controller]")]
public class LeagueController : ControllerBase
{
    private readonly ILogger<LeagueController> _logger;
    private readonly IGenericIntRepository<LeagueModel> _leagueRepository;

    public LeagueController(ILogger<LeagueController> logger, IGenericIntRepository<LeagueModel> leagueRepository)
    {
        _logger = logger;
        _leagueRepository = leagueRepository;
    }

    [HttpGet("GetAllLeagues")]
    public IEnumerable<LeagueModel> GetAllLeagues()
    {
        try
        {
            return _leagueRepository.GetAll();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    [HttpGet("GetLeague/{id}")]
    public LeagueModel? GetLeague([FromRoute] int id)
    {
        try
        {
            return _leagueRepository.GetById(id);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    [HttpPost("AddLeague")]
    public void AddLeague([FromBody] LeagueModel model)
    {
        try
        {
            _leagueRepository.Add(model);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpDelete("DeleteLeague/{id}")]
    public bool DeleteLeague([FromRoute] int id)
    {
        try
        {
            return _leagueRepository.Delete(id);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
}