﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Daos;
using WebAPI.Enums;
using WebAPI.Managers;
using WebAPI.Models.User;

namespace WebAPI.Controllers;

[Route("[controller]")]
[ApiController]
public class AuthController : ControllerBase
{
    private readonly UserManager _userManager;
    private readonly IUserRepository _userRepository;
    public AuthController(UserManager userManager, IUserRepository userRepository)
    {
        _userManager = userManager;
        _userRepository = userRepository;
    }
    
    [HttpPost("register")]
    public ActionResult<bool> Register(UserRegisterModel request)
    {
        try
        {
            bool res = _userManager.Register(request);

            if (!res)
            {
                return BadRequest("Username is already used");
            }
        
            return Ok();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    [HttpPost("login")]
    public ActionResult<ConnectionResultModel> Login(UserConnectionModel request)
    {
        try
        {
            var connectionResult = _userManager.Login(request);

            if (connectionResult.Token == "")
            {
                return BadRequest("Incorrect password or username");
            }
            
            return Ok(connectionResult);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("users")]
    [Authorize(Roles = RoleRestriction.Admin)]
    public ActionResult<List<UserModel>> GetUsers()
    {
        try
        {
            var users = _userManager.GetUsers();

            return Ok(users);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpDelete("user/{userId}")]
    [Authorize(Roles = RoleRestriction.Admin)]
    public ActionResult<List<UserModel>> DeleteUser([FromRoute] string userId)
    {
        try
        {
            var result = _userManager.DeleteUser(int.Parse(userId));

            if (result)
            {
                return Ok();
            }

            return BadRequest();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("currency/{userId}")]
    [Authorize(Roles = RoleRestriction.User)]
    public ActionResult<float> GetCurrency([FromRoute] int userId)
    {
        try
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity == null)
            {
                return BadRequest(0);
            }
            
            string? userName = identity.FindFirst(ClaimTypes.Name)?.Value;

            if (userName == null)
            {
                return BadRequest(0);
            }

            var user = _userRepository.GetUserByName(userName);

            if (user == null)
            {
                return BadRequest(0);
            }

            if (user.Id != userId)
            {
                return Unauthorized(0);
            }
            
            float result = _userManager.GetCurrency(userId);
            
            return Ok(result);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}