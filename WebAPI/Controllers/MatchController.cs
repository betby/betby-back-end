﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Daos;
using WebAPI.Enums;
using WebAPI.Models;

namespace WebAPI.Controllers;

[ApiController]
[Route("[controller]")]
public class MatchController : ControllerBase
{
    private readonly ILogger<MatchController> _logger;
    private readonly IMatchRepository _matchRepository;

    public MatchController(ILogger<MatchController> logger, IMatchRepository matchRepository)
    {
        _logger = logger;
        _matchRepository = matchRepository;
    }

    [HttpGet("GetAllMatches")]
    public IEnumerable<MatchModel> GetAllMatches()
    {
        try
        {
            return _matchRepository.GetAll();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("GetPastMatches/{leagueId}")]
    public IEnumerable<MatchModel> GetPastMatches([FromRoute] int leagueId)
    {
        try
        {
            return _matchRepository.GetAllBeforeDate(DateTime.Today, leagueId);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("GetFutureMatches/{leagueId}")]
    public IEnumerable<MatchModel> GetFutureMatches([FromRoute] int leagueId)
    {
        try
        {
            return _matchRepository.GetAllAfterDate(DateTime.Today, leagueId);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("GetCurrentMatches/{leagueId}")]
    public IEnumerable<MatchModel> GetCurrentMatches([FromRoute] int leagueId)
    {
        try
        {
            return _matchRepository.GetAllByDate(DateTime.Today.ToString(), leagueId);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("GetAllMatchesByTeamId/{id}")]
    public IEnumerable<MatchModel> GetAllMatchesByTeamId([FromRoute] int id)
    {
        try
        {
            return _matchRepository.GetAllByTeamId(id);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("GetMatch/{id}")]
    public MatchModel? GetMatch([FromRoute] int id)
    {
        try
        {
            return _matchRepository.GetById(id);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpPost("AddMatch")]
    public void AddMatch([FromBody] MatchModel model)
    {
        try
        {
            _matchRepository.Add(model);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpDelete("DeleteMatch/{id}")]
    public bool DeleteMatch([FromRoute] int id)
    {
        try
        {
            return _matchRepository.Delete(id);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}