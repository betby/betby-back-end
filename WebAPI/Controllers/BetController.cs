using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Daos;
using WebAPI.Enums;
using WebAPI.Managers;
using WebAPI.Models;
using WebAPI.Models.User;

namespace WebAPI.Controllers;

[ApiController]
[Route("[controller]")]

public class BetController : ControllerBase
{
    private readonly ILogger<BetController> _logger;
    private readonly BetManager _betManager;
    private readonly IUserRepository _userRepository;

    public BetController(ILogger<BetController> logger, BetManager betManager, IUserRepository userRepository)
    {
        _logger = logger;
        _betManager = betManager;
        _userRepository = userRepository;
    }
    
    [HttpGet("GetAllBets")]
    [Authorize(Roles = RoleRestriction.Admin)]
    public ActionResult<IEnumerable<BetModel>> GetAllBets()
    {
        try
        {
            return Ok(_betManager.GetAllBets());
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("GetAllBetsByUserId/{id}")]
    [Authorize(Roles = RoleRestriction.User)]
    public ActionResult<IEnumerable<BetModel>> GetAllBetsByUserId([FromRoute] int id)
    {
        try
        {
            var user = CheckUserClaimValidity();

            if (user == null)
            {
                return  BadRequest(new List<BetModel>());
            }

            if (user.Id != id)
            {
                return BadRequest(new List<BetModel>());
            }
            
            return Ok(_betManager.GetAllBetsByUserId(id));
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("GetBetById/{id}")]
    [Authorize(Roles = RoleRestriction.User)]
    public ActionResult<BetModel?> GetBet([FromRoute] int id)
    {
        try
        {
            var user = CheckUserClaimValidity();

            if (user == null)
            {
                return BadRequest(null);
            }
            
            var betResult = _betManager.GetBet(id);

            if (betResult == null)
            {
                return BadRequest(null);
            }
            
            if (user.Id != betResult.UserId)
            {
                return BadRequest(null);
            }

            return Ok(betResult);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    [HttpPost("AddBet")]
    [Authorize(Roles = RoleRestriction.User)]
    public ActionResult<BetModel> Add([FromBody] BetRequestModel model)
    {
        try
        {
            BetModel badBet = new BetModel()
            {
                BetAccepted = false
            };
            
            var user = CheckUserClaimValidity();

            if (user == null)
            {
                return BadRequest(badBet);
            }

            if (user.Id != model.UserId)
            {
                return BadRequest(badBet);
            }
            
            return Ok(_betManager.Add(model));
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpDelete("DeleteBet/{id}")]
    [Authorize(Roles = RoleRestriction.Admin)]
    public ActionResult<bool> DeleteBet([FromRoute] int id)
    {
        try
        {
            var result = _betManager.DeleteBet(id);
            if (result)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    private UserModel? CheckUserClaimValidity()
    {
        var identity = HttpContext.User.Identity as ClaimsIdentity;
        if (identity == null)
        {
            return null;
        }
            
        string? userName = identity.FindFirst(ClaimTypes.Name)?.Value;

        if (userName == null)
        {
            return null;
        }

        var user = _userRepository.GetUserByName(userName);

        return user;
    }
}