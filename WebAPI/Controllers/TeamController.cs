using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Daos;
using WebAPI.Enums;
using WebAPI.Models;


namespace WebAPI.Controllers;

[ApiController]
[Route("[controller]")]

public class TeamController : ControllerBase
{
    private readonly ILogger<TeamController> _logger;
    private readonly ITeamRepository _teamRepository;
    
    public TeamController(ILogger<TeamController> logger, ITeamRepository teamRepository)
    {
        _logger = logger;
        _teamRepository = teamRepository;
    }
    
    [HttpGet("GetAllTeams")]
    public IEnumerable<TeamModel> GetAllTeams()
    {
        try
        {
            return _teamRepository.GetAll();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("GetAllTeamsByLeagueId/{id}")]
    public IEnumerable<TeamModel> GetAllTeamsByLeagueId([FromRoute] int id)
    {
        try
        {
            return _teamRepository.GetAllTeamsByLeagueId(id);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpGet("GetTeam/{id}/{leagueId}")]
    public TeamModel? GetTeam([FromRoute] int id, [FromRoute] int leagueId)
    {
        try
        {
            return _teamRepository.GetById(id, leagueId);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    [HttpPost("AddTeam")]
    public void Add([FromBody] TeamModel model)
    {
        try
        {
            _teamRepository.Add(model);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    [HttpDelete("DeleteTeam/{id}/{leagueId}")]
    public bool DeleteTeam([FromRoute] int id, [FromRoute] int leagueId)
    {
        try
        {
            return _teamRepository.Delete(id, leagueId);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}